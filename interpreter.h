#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <map>
#include <stack>
#include <iostream>
#include <exception>
#include "parser.h"

using std::stack;
using std::cin;
using std::cout;
using std::endl;
using std::map;
using std::exception;

struct ReturnValue: public exception{
    ReturnValue(int value):
        value(value)
    {}

    int getValue() const{
        return value;
    }

private:
    int value;
};

struct Interpreter: public Visitor{
    static void interprete(ProgramContext pc){
        Interpreter interpreter;
        interpreter.entryPoint = pc.entryPoint;
        interpreter.functions = pc.functions;
        interpreter.entryPoint->accept(interpreter);
    }

    int visit(FunDef const &node);
    int visit(FunCall const &node);
    int visit(VarDef const &node);
    int visit(Var const &node);
    int visit(Num const &node);
    int visit(Operator const &node);
    int visit(If const &node);
    int visit(While const &node);
    int visit(Cond const &node);
    int visit(Return const &node);
    int visit(Read const &node);
    int visit(Print const &node);

private:
    InstructionPtr entryPoint;
    map<string, FunPtr> functions;
    map<string, int> globalVars;
    stack< map<string, int> > localVars;

    bool getVarValue(string const &var, int &value) const;
    void setVarValue(string const &var, int value);

    int interpretInstructions(Instructions instructions);
};

#endif // INTERPRETER_H
