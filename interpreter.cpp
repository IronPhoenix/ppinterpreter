#include "interpreter.h"

bool Interpreter::getVarValue(string const &var, int &value) const{
    if (!localVars.empty()) {
        map<string, int>::const_iterator localVarIt = localVars.top().find(var);
        if (localVarIt != localVars.top().end()) {
            value = localVarIt->second;
            return true;
        }
    }
    map<string, int>::const_iterator globalVarIt = globalVars.find(var);
    if (globalVarIt != globalVars.end()) {
        value = globalVarIt->second;
        return true;
    }
    return false;
}

void Interpreter::setVarValue(string const &var, int value){
    if (!localVars.empty()) localVars.top()[var] = value;
    else globalVars[var] = value;
}

int Interpreter::interpretInstructions(Instructions instructions){
    int value = 0;
    for (Instructions::const_iterator it = instructions.begin(); it != instructions.end(); ++it)
        value = (*it)->accept(*this);
    return value;
}

int Interpreter::visit(FunDef const &node){
    try {
        interpretInstructions(node.getInstructions());
    } catch (ReturnValue const &returnValue) {
        return returnValue.getValue();
    }
    return 0;
}

int Interpreter::visit(FunCall const &node){
    map<string, FunPtr>::iterator functionIt = functions.find(node.getName());
    if (functionIt == functions.end()) throw Error("Undefined function " + node.getName(), node.getLineNumber());

    FunPtr function = functionIt->second;
    vector<string> const &params = function->getParams();
    Instructions const &args = node.getParams();
    if (args.size() != params.size()) throw Error("Arguments number mismatch", node.getLineNumber());

    map<string, int> functionLocals;
    for (Instructions::const_iterator it = args.begin(); it != args.end(); ++it)
        functionLocals[params[it - args.begin()]] = (*it)->accept(*this);

    localVars.push(functionLocals);
    int res = function->accept(*this);
    localVars.pop();

    return res;
}

int Interpreter::visit(VarDef const &node){
    int value = node.getExp()->accept(*this);
    setVarValue(node.getName(), value);
    return value;
}

int Interpreter::visit(Var const &node){
    int value = 0;
    if (!getVarValue(node.getName(), value)) throw Error("Undefined variable " + node.getName(), node.getLineNumber());
    return value;
}

int Interpreter::visit(Num const &node){
    return node.getValue();
}

int Interpreter::visit(Operator const &node){
    int left  = node.getLeft()->accept(*this);
    int right = node.getRight()->accept(*this);
    switch (node.getOperation()) {
    case '+': return left + right;
    case '-': return left - right;
    case '*': return left * right;
    case '/':{
        if (right == 0) throw Error("Division by zero", node.getLineNumber());
        return left / right;
    }
    default: throw Error("Runtime error", node.getLineNumber());
    }
    return 0;
}

int Interpreter::visit(If const &node){
    int value = node.getCond()->accept(*this);
    if (value != 0)
        value = interpretInstructions(node.getInstructions());
    return value;
}

int Interpreter::visit(While const &node){
    int cond = 1;
    int value = 0;
    while ((cond = node.getCond()->accept(*this)))
        value = interpretInstructions(node.getInstructions());
    return value;
}

int Interpreter::visit(Cond const &node){
    int left  = node.getLeft()->accept(*this);
    int right = node.getRight()->accept(*this);
    if(node.getComparison() == "==") return left == right;
    if(node.getComparison() == "!=") return left != right;
    if(node.getComparison() == "<") return left < right;
    if(node.getComparison() == "<=") return left <= right;
    if(node.getComparison() == ">") return left > right;
    if(node.getComparison() == ">=") return left >= right;
    throw Error("Runtime error", node.getLineNumber());
    return 0;
}

int Interpreter::visit(Return const &node){
    int value = node.getExp()->accept(*this);
    throw ReturnValue(value);
}

int Interpreter::visit(Read const &node){
    int value = 0;
    cin >> value;
    setVarValue(node.getVar(), value);
    return value;
}

int Interpreter::visit(Print const &node){
    int value = node.getExp()->accept(*this);
    cout << value << endl;
    return value;
}
