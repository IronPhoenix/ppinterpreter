#ifndef PARSER_H
#define PARSER_H

#include "ast.h"
#include "lexer.h"
#include "programContext.h"
#include "error.h"

struct Parser
{
    Parser(istream &sourceStream):
        lexer(sourceStream)
    {}

    ProgramContext parse(){
        return parseProgram();
    }

private:
    Lexer lexer;

    size_t getLine() const{
        return lexer.getLineNumber();
    }

    ProgramContext parseProgram();
    InstructionPtr parseInstruction();
    InstructionPtr parsePlusMinus();
    InstructionPtr parseMultDiv();
    InstructionPtr parseId();
    InstructionPtr parseNum();
    InstructionPtr parseVarDef();
    InstructionPtr parseValue();
    InstructionPtr parseIf();
    InstructionPtr parseWhile();
    InstructionPtr parseReturn();
    InstructionPtr parseCond();
    InstructionPtr parseRead();
    InstructionPtr parsePrint();
    FunPtr parseFunDef();
};

#endif // PARSER_H
