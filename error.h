#ifndef ERROR_H
#define ERROR_H

#include <stdexcept>
#include <string>

using std::string;
using std::runtime_error;

struct Error: public runtime_error{
    Error(string const &msg, size_t lineNumber):
        runtime_error(msg),
        lineNumber(lineNumber)
    {}

    size_t getLineNumber() const{
        return lineNumber;
    }

private:
    size_t lineNumber;
};

#endif // ERROR_H
