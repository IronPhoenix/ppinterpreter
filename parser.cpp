#include "parser.h"

ProgramContext Parser::parseProgram(){
    Instructions instructions;
    map<string, FunPtr> functions;
    while (!lexer.checkToken(Token::Eof)) {
        InstructionPtr instruction = parseInstruction();
        if (instruction) {
            instructions.push_back(instruction);
            continue;
        }

        FunPtr funDef = parseFunDef();
        if (funDef) {
            functions[funDef->getName()] = funDef;
            continue;
        }
    }
    size_t firstInstructionLineNumber = 0;
    if(instructions.size() != 0) firstInstructionLineNumber = instructions.at(0)->getLineNumber();
    return ProgramContext(InstructionPtr(new FunDef("", vector<string>(), instructions, firstInstructionLineNumber)), functions);
}

InstructionPtr Parser::parseInstruction(){
    while (lexer.checkToken(Token::CR)) lexer.nextToken();

    InstructionPtr res = parseRead();
    if (!res) res = parsePrint();
    if (!res) res = parseVarDef();
    if (!res) res = parsePlusMinus();
    if (!res) res = parseIf();
    if (!res) res = parseWhile();
    if (!res) res = parseReturn();
    if (res) while (lexer.checkToken(Token::CR)) lexer.nextToken();

    return res;
}

InstructionPtr Parser::parsePlusMinus(){
    InstructionPtr left = parseMultDiv();
    if (!left) return InstructionPtr();

    if (!lexer.checkToken(Token::PLUS) && !lexer.checkToken(Token::MINUS)) return left;
    Token op = lexer.nextToken();

    InstructionPtr right = parsePlusMinus();
    if (!right) throw Error("syntax error", lexer.getLineNumber());

    switch (op.type) {
    case Token::PLUS: return InstructionPtr(new Operator('+', left, right, lexer.getLineNumber()));
    case Token::MINUS: return InstructionPtr(new Operator('-', left, right, lexer.getLineNumber()));
    default: throw Error("syntax error", lexer.getLineNumber());
    }
}

InstructionPtr Parser::parseMultDiv(){
    InstructionPtr left = parseValue();
    if (!left) return InstructionPtr();

    if (!lexer.checkToken(Token::MULT) && !lexer.checkToken(Token::DIV)) return left;
    Token op = lexer.nextToken();

    InstructionPtr right = parseMultDiv();
    if (!right) throw Error("syntax error", lexer.getLineNumber());

    switch (op.type) {
    case Token::MULT: return InstructionPtr(new Operator('*', left, right, lexer.getLineNumber()));
    case Token::DIV: return InstructionPtr(new Operator('/', left, right, lexer.getLineNumber()));
    default: throw Error("syntax error", lexer.getLineNumber());
    }
}

InstructionPtr Parser::parseId(){
    if (!lexer.checkToken(Token::ID)) return InstructionPtr();

    string id = lexer.nextToken().name;
    if (!lexer.checkToken(Token::LP)) return InstructionPtr(new Var(id, lexer.getLineNumber()));
    lexer.nextToken();

    Instructions functionParams;
    if (!lexer.checkToken(Token::RP)) {
        Token t;
        do {
            InstructionPtr p = parsePlusMinus();
            if (!p) throw Error("syntax error", lexer.getLineNumber());
            functionParams.push_back(p);
            t = lexer.nextToken();
        } while (t.type == Token::COM);
        if (t.type != Token::RP) throw Error("syntax error", lexer.getLineNumber());
    }
    else lexer.nextToken();

    return InstructionPtr(new FunCall(id, functionParams, lexer.getLineNumber()));
}

InstructionPtr Parser::parseNum(){
    if (lexer.checkToken(Token::NUM)) {
        Token t = lexer.nextToken();
        return InstructionPtr(new Num(t.value, lexer.getLineNumber()));
    }
    return InstructionPtr();
}

InstructionPtr Parser::parseVarDef(){
    if (!lexer.checkToken(Token::ID) || lexer.checkToken(Token::LP, 2)) return InstructionPtr();
    string id = lexer.nextToken().name;

    if (!lexer.checkToken(Token::ASGN)) throw Error("syntax error", lexer.getLineNumber());
    lexer.nextToken();

    InstructionPtr exp = parsePlusMinus();
    if (!exp) throw Error("syntax error", lexer.getLineNumber());

    return InstructionPtr(new VarDef(id, exp, lexer.getLineNumber()));
}

InstructionPtr Parser::parseValue(){
    if (lexer.checkToken(Token::MINUS)) {
        lexer.nextToken();
        InstructionPtr val = parseValue();
        if (!val) throw Error("syntax error", lexer.getLineNumber());
        InstructionPtr zero = InstructionPtr(new Num(0, 0));
        return InstructionPtr(new Operator('-', zero, val, lexer.getLineNumber()));
    }

    if (lexer.checkToken(Token::LP)) {
        lexer.nextToken();
        InstructionPtr exp = parsePlusMinus();
        if (!exp || !lexer.checkToken(Token::RP)) throw Error("syntax error", lexer.getLineNumber());
        lexer.nextToken();
        return exp;
    }

    InstructionPtr val = parseNum();
    if (!val) val = parseId();

    return val;
}

InstructionPtr Parser::parseIf(){
    if (!lexer.checkToken(Token::IF)) return InstructionPtr();
    lexer.nextToken();

    InstructionPtr cond = parseCond();
    if (!cond || !lexer.checkToken(Token::COL) || !lexer.checkToken(Token::CR, 2)) throw Error("syntax error", lexer.getLineNumber());
    lexer.nextToken();
    lexer.nextToken();

    Instructions instructions;
    while (!lexer.checkToken(Token::END)) {
        InstructionPtr instruction = parseInstruction();
        if (!instruction) throw Error("syntax error", lexer.getLineNumber());
        instructions.push_back(instruction);
    }
    lexer.nextToken();

    return InstructionPtr(new If(cond, instructions, lexer.getLineNumber()));
}

InstructionPtr Parser::parseWhile(){
    if (!lexer.checkToken(Token::WHILE)) return InstructionPtr();
    lexer.nextToken();

    InstructionPtr cond = parseCond();
    if (!cond || !lexer.checkToken(Token::COL) || !lexer.checkToken(Token::CR, 2)) throw Error("syntax error", lexer.getLineNumber());
    lexer.nextToken();
    lexer.nextToken();

    Instructions instructions;
    while (!lexer.checkToken(Token::END)) {
        InstructionPtr instruction = parseInstruction();
        if (!instruction) throw Error("syntax error", lexer.getLineNumber());
        instructions.push_back(instruction);
    }
    lexer.nextToken();

    return InstructionPtr(new While(cond, instructions, lexer.getLineNumber()));
}

InstructionPtr Parser::parseReturn(){
    if (!lexer.checkToken(Token::RET)) return InstructionPtr();
    lexer.nextToken();

    InstructionPtr exp = parsePlusMinus();
    if (!exp) throw Error("syntax error", lexer.getLineNumber());
    return InstructionPtr(new Return(exp, lexer.getLineNumber()));
}

InstructionPtr Parser::parseCond(){
    InstructionPtr left = parsePlusMinus();
    if (!left) return InstructionPtr();

    Token ct = lexer.nextToken();
    if (ct.type != Token::EQ && ct.type != Token::NE && ct.type != Token::LT && ct.type != Token::GT && ct.type != Token::LE && ct.type != Token::GE)
        throw Error("syntax error", lexer.getLineNumber());

    InstructionPtr right = parsePlusMinus();
    if (!right) throw Error("syntax error", lexer.getLineNumber());

    string sign;
    switch (ct.type) {
    case Token::EQ: sign = "=="; break;
    case Token::NE: sign = "!="; break;
    case Token::GT: sign = ">"; break;
    case Token::LT: sign = "<"; break;
    case Token::GE: sign = ">="; break;
    case Token::LE: sign = "<="; break;
    };

    return InstructionPtr(new Cond(left, right, sign, lexer.getLineNumber()));
}

InstructionPtr Parser::parseRead(){
    if (!lexer.checkToken(Token::READ)) return InstructionPtr();
    lexer.nextToken();

    Token var = lexer.nextToken();
    if (var.type != Token::ID) throw Error("syntax error", lexer.getLineNumber());

    return InstructionPtr(new Read(var.name, lexer.getLineNumber()));
}

InstructionPtr Parser::parsePrint(){
    if (!lexer.checkToken(Token::PRINT)) return InstructionPtr();
    lexer.nextToken();

    InstructionPtr exp = parsePlusMinus();
    if (!exp) throw Error("syntax error", lexer.getLineNumber());

    return InstructionPtr(new Print(exp, lexer.getLineNumber()));
}

FunPtr Parser::parseFunDef(){
    if (!lexer.checkToken(Token::DEF)) return FunPtr();
    lexer.nextToken();

    Token functionName = lexer.nextToken();
    if (functionName.type != Token::ID || !lexer.checkToken(Token::LP)) throw Error("syntax error", lexer.getLineNumber());
    lexer.nextToken();

    vector<string> functionParams;
    if (!lexer.checkToken(Token::RP)) {
        Token t;
        do {
            Token p = lexer.nextToken();
            if (p.type != Token::ID) throw Error("syntax error", lexer.getLineNumber());
            functionParams.push_back(p.name);
            t = lexer.nextToken();
        } while(t.type == Token::COM);
        if (t.type != Token::RP) throw Error("syntax error", lexer.getLineNumber());
    }
    else lexer.nextToken();

    if (!lexer.checkToken(Token::COL) || !lexer.checkToken(Token::CR, 2)) throw Error("syntax error", lexer.getLineNumber());
    lexer.nextToken();
    lexer.nextToken();

    Instructions instructions;
    while (!lexer.checkToken(Token::END)) {
        InstructionPtr instruction = parseInstruction();
        instructions.push_back(instruction);
        continue;
    }
    lexer.nextToken();

    if (!lexer.checkToken(Token::CR)) throw Error("syntax error", lexer.getLineNumber());
    lexer.nextToken();

    return FunPtr(new FunDef(functionName.name, functionParams, instructions, lexer.getLineNumber()));
}
