#include <iostream>
#include <fstream>
#include "error.h"
#include "interpreter.h"

using std::ifstream;
using std::cout;
using std::endl;

int main(int args, char const *argv[])
{
    if (args < 2){
        cout << "Usage: " << argv[0] << " <SOURCE_FILE_NAME>" << endl;
        return 1;
    }

    ifstream in(argv[1]);
    if(!in.good()){
        cout << "File " << argv[1] << " does not exist" << endl;
        in.close();
        return 2;
    }
    in >> std::noskipws;
    try{
        Parser parser(in);
        ProgramContext pc = parser.parse();
        Interpreter::interprete(pc);
    } catch(Error const &error){
        cout << "Line " << error.getLineNumber() << ": " << error.what() << endl;
    }
    return 0;
}

