default: ppinterpreter

ppinterpreter: main.o lexer.o parser.o interpreter.o
	g++ main.o lexer.o parser.o interpreter.o -o ppinterpreter

main.o: main.cpp
	g++ -c main.cpp

lexer.o: lexer.cpp
	g++ -c lexer.cpp

parser.o: parser.cpp
	g++ -c parser.cpp

interpreter.o: interpreter.cpp
	g++ -c interpreter.cpp

clean:
	rm -rf *.o ppinterpreter